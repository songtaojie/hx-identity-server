using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Hx.IdentityServer.Common;
using Hx.IdentityServer.Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Events;
using Serilog.Sinks.SystemConsole.Themes;

namespace Hx.IdentityServer
{
    public class Program
    {
        public static int Main(string[] args)
        {
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
                .MinimumLevel.Override("Microsoft.Hosting.Lifetime", LogEventLevel.Information)
                .MinimumLevel.Override("System", LogEventLevel.Warning)
                .MinimumLevel.Override("Microsoft.AspNetCore.Authentication", LogEventLevel.Information)
                .Enrich.FromLogContext()
                // uncomment to write to Azure diagnostics stream
                .WriteTo.File($"log/{DateTime.Now:yyyy-MM-dd}.log",
                              fileSizeLimitBytes: 1_000_000,
                              rollOnFileSizeLimit: true,
                              shared: true,
                              flushToDiskInterval: TimeSpan.FromSeconds(1))
                .WriteTo.Console(outputTemplate: "[{Timestamp:HH:mm:ss} {Level}] {SourceContext}{NewLine}{Message:lj}{NewLine}{Exception}{NewLine}", theme: AnsiConsoleTheme.Code)
                .CreateLogger();

            try
            {
                var seed = args.Contains("/seed");
                if (seed)
                {
                    args = args.Except(new[] { "/seed" }).ToArray();
                }
                var host = CreateHostBuilder(args).Build();
                if (seed)
                {
                    SeedData.EnsureSeedData(host.Services);
                    return 1;
                }

                ConsoleHelper.WriteSuccessLine("应用程序启动成功");
                host.Run();
                return 1;
            }
            catch (Exception ex)
            {
                ConsoleHelper.WriteErrorLine(string.Format("异常消息：{0}", ex.Message));
                ConsoleHelper.WriteErrorLine(string.Format("堆栈StackTrace：{0}", ex.StackTrace));
                Log.Fatal(ex, "Host terminated unexpectedly.");
                return 1;
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .UseSerilog()
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>()
                    .UseUrls("http://*:5002");
                });
    }
}
